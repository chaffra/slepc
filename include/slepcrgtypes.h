/*
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   SLEPc - Scalable Library for Eigenvalue Problem Computations
   Copyright (c) 2002-2017, Universitat Politecnica de Valencia, Spain

   This file is part of SLEPc.
   SLEPc is distributed under a 2-clause BSD license (see LICENSE).
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*/

#if !defined(__SLEPCRGTYPES_H)
#define __SLEPCRGTYPES_H

/*S
   RG - Region of the complex plane.

   Level: beginner

.seealso: RGCreate()
S*/
typedef struct _p_RG* RG;

#endif
